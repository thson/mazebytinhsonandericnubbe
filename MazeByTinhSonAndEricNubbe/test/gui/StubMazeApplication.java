package gui;

public class StubMazeApplication{

	private static final long serialVersionUID = 1L;
	public Controller controller;
	testSpecs test;
	
	public StubMazeApplication(testSpecs test) {
		this.test = test;
	}
	
	public Controller createController(testSpecs test) {
		this.controller = new StubController(test);
		return controller;
	}
	
}
 