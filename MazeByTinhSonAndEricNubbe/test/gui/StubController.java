package gui;

import generation.MazeConfiguration;
import generation.Order.Builder;

public class StubController extends Controller{
	private testSpecs config;
	private MazeConfiguration mazeConfig;
    public StubController(testSpecs test) {
       super();
       this.config = test;
       this.start();
    }
    
    @Override
    public void start() { 
    	super.setDeterministic();
    	super.start();
    	super.setBuilder(config.getBuilder());
		super.setPerfect(config.isPerfect());
        super.setRobotAndDriver(config.getRobot(), config.getDriver());
    	switchFromTitleToGenerating(9);
    }
    
    @Override
    public void switchFromTitleToGenerating(int skillLevel) {
        currentState = states[1];
        currentState.setSkillLevel(skillLevel);
        currentState.setPerfect(perfect);
        this.currentState.start(this, panel);
		System.out.println(this.getMazeConfiguration());
    }
    
    @Override
	 public void switchFromGeneratingToPlaying(MazeConfiguration config) {
      
       currentState = states[2];
       currentState.setMazeConfiguration(config);
       super.robot = new BasicRobot();
       super.robot.setMaze(this);
       super.robot.getMazeConfiguration();
       currentState.start(this, panel);
   }
    
    public RobotDriver passDriver() {
    	return super.getDriver();
    }
    
    public Builder getBuilder() {
    	return config.getBuilder();
    }
        
}
