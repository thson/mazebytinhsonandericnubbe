package gui;

import generation.Order;

public class testSpecs {
	private Order.Builder builder;
	private int level;
	private boolean perfect;
	private BasicRobot robot;
	private RobotDriver driver;
	
	public Order.Builder getBuilder() {
		return builder;
	}
	public void setBuilder(Order.Builder builder) {
		this.builder = builder;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public boolean isPerfect() {
		return perfect;
	}
	public void setPerfect(boolean perfect) {
		this.perfect = perfect;
	}
	public BasicRobot getRobot() {
		return robot;
	}
	public void setRobot(BasicRobot robot) {
		this.robot = robot;
	}
	public RobotDriver getDriver() {
		return driver;
	}
	public void setDriver(RobotDriver driver) {
		this.driver = driver;
	}
}
