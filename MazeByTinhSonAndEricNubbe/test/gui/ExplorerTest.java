package gui;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import generation.MazeConfiguration;
import generation.MazeFactory;
import generation.Order.Builder;
import generation.StubOrder;

/**this test should ideally covers all three algorithms since the main point was to see if robot can exit the maze*/
public class ExplorerTest {
	RobotDriver driver;
	BasicRobot robot;
	Controller controller;
	StubOrder stubOrder; //use to get mazeConfiguration
	MazeFactory mazeFactory;
	MazeConfiguration mazeConfig;
	StatePlaying sp;
	 
	public void init(int level) {
		mazeFactory = new MazeFactory(true);
		stubOrder = new StubOrder(Builder.Eller, level, true);
		mazeFactory.order(stubOrder);
		mazeFactory.waitTillDelivered(); //order is important.
		mazeConfig = stubOrder.getSetUp();
		mazeConfig.setMazedists(mazeConfig.getMazedists());
		this.robot = new BasicRobot();
		this.driver = new Explorer(robot);
		driver.setRobot(robot);
		controller = new Controller();
		sp = controller.getPlayingState();
		sp.setMazeConfiguration(mazeConfig);
		sp.setCurrentPosition(mazeConfig.getStartingPosition()[0], mazeConfig.getStartingPosition()[1]);
		sp.setCurrentDirection(1, 1);
		robot.setMaze(controller);
		robot.setBatteryLevel(Integer.MAX_VALUE); 
		controller.setRobotAndDriver(robot, driver);
		driver.setDimensions(mazeConfig.getWidth(), mazeConfig.getHeight());
		driver.setDistance(mazeConfig.getMazedists());
		driver.setDimensions(mazeConfig.getWidth(), mazeConfig.getHeight());
//		controller.turnOffGraphics();
	}

	
	@Test public void testBasicCharacteristics() {
		init(0);
		assertNotNull(robot);
		assertNotNull(driver);
		assertEquals(Integer.MAX_VALUE, 0, robot.getBatteryLevel());
		assertEquals(Integer.MAX_VALUE, 0, driver.getEnergyConsumption());
		
	}
	@Test public void testDriveToExitlevel2() {
		init(2);
		try {
			assertTrue(driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	} 
	
//	@Test public void testDriveToExitlevel3() {
//		init(3);
//		try {
//			assertTrue(driver.drive2Exit());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	
//	@Test public void testDriveToExitlevel4() {
//		init(4);
//		try {
//			assertTrue(driver.drive2Exit());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block 
//			e.printStackTrace();
//		}
//	}
//	 
//	@Test public void testDriveToExitlevel5() {
//		init(5);
//		try {
//			assertTrue(driver.drive2Exit());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	@Test public void testDriveToExitlevel6() {
		init(6);
		try {
			assertTrue(driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	@Test public void testDriveToExitlevel7() {
//		init(7);
//		try {
//			assertTrue(driver.drive2Exit());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	@Test public void testDriveToExitlevel8() {
		init(8);
		try {
			assertTrue(driver.drive2Exit());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	@Test public void testDriveToExitlevel9() {
//		init(9);
//		try {
//			assertTrue(driver.drive2Exit());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
}