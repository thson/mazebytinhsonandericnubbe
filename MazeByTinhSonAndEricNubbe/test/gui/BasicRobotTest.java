package gui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import generation.MazeConfiguration;
import generation.MazeFactory;
import generation.Order.Builder;
import generation.StubOrder;
import gui.Robot.Direction;
import gui.Robot.Turn;
import gui.Robot.UnsupportedOperationException;
/**This test class assesses the characteristics of the robot class in the gui folder. This includes setting battery levels, get correct odometer and battery readings, as well as checking room sensor and distanceToObstacle methods being tested by always
 * having the robot facing the wall in the beginning. This is pretty much a white box test since information about the maze is needed with respect to the robot's position. 
 * @author Tinh Son, Eric Nubbe.
 *
 */
public class BasicRobotTest extends Controller{
	RobotDriver driver;
	BasicRobot robot;
	Controller controller;
	StubOrder stubOrder; //use to get mazeConfiguration
	MazeFactory mazeFactory;
	MazeConfiguration mazeConfig;
	StatePlaying sp;
	
	
	@Before
	public void init() throws Exception{
		mazeFactory = new MazeFactory(true);
		stubOrder = new StubOrder(Builder.Eller, 4, false);
		mazeFactory.order(stubOrder);
		mazeFactory.waitTillDelivered(); //order is important.
		mazeConfig = stubOrder.getSetUp();
		mazeConfig.setMazedists(mazeConfig.getMazedists());
		this.robot = new BasicRobot();
		this.driver = new ManualDriver(robot);
		driver.setRobot(robot);
		controller = new Controller();
		sp = controller.getPlayingState();
		sp.setMazeConfiguration(mazeConfig);
		robot.setMaze(controller);
		robot.setBatteryLevel(3000);
		controller.setRobotAndDriver(robot, driver);
		controller.turnOffGraphics();
	}
	 
	@Test public void testIsRobotInsideRoom() {
		try {
			assertFalse(robot.isInsideRoom());
		} catch (UnsupportedOperationException e) {
			System.out.println(e);
		}
	}
	
	@Test public void testDoesRobotHaveRoomSensor(){
		assertTrue(robot.hasRoomSensor());
	}
	
	@Test public void testRobotHasJuice(){
		assertEquals(robot.getBatteryLevel(), 0, 3000);
	}
	@Test public void testDistanceToObstacleRight() throws UnsupportedOperationException {
		assertEquals(robot.distanceToObstacle(Direction.RIGHT), 0 , 0);
	}
	@Test public void testSimpleMoveandHelperMethods() {
		robot.move(1,true);
		assertEquals(robot.getBatteryLevel(), 0 , 2995);
		assertEquals(robot.getOdometerReading(),0, 1);
	}
	@Test public void testLargerMoveandHelperMethods() {
		robot.move(2,true);
		assertEquals(robot.getBatteryLevel(), 0 , 2990);
		assertEquals(robot.getOdometerReading(), 0, 2);
	}
	@Test public void testCurrentPosition() throws Exception {
		assertEquals(robot.getCurrentPosition()[0], 0,0);
		assertEquals(robot.getCurrentPosition()[1],0,0);
		robot.resetOdometer();
		assertEquals(0, 0, robot.getOdometerReading());
	}
	@Test public void testRobotHasDistanceSensor() {
		assertTrue(robot.hasDistanceSensor(Direction.FORWARD));
		assertTrue(robot.hasDistanceSensor(Direction.BACKWARD));
		assertTrue(robot.hasDistanceSensor(Direction.LEFT));
		assertTrue(robot.hasDistanceSensor(Direction.RIGHT));	
	}
	@Test public void testgetMethods() {
		assertEquals(robot.getEnergyForFullRotation(), 0, 12);
		assertEquals(robot.getEnergyForStepForward(), 0 , 5);
		robot.setBatteryLevel(3000);
		assertEquals(3000, 0, robot.getBatteryLevel());
		
	}
	
	@Test public void testDistancetoObstacle() throws UnsupportedOperationException {
		assertEquals(robot.distanceToObstacle(Direction.FORWARD), 0, 1 );
	}
	 
	@Override 
	 public void switchFromTitleToGenerating(int skillLevel) {
        currentState = states[1];
        currentState.setSkillLevel(skillLevel);
        currentState.setPerfect(perfect); 
        currentState.start(this, panel);
    }
	
	@Override
	 public void switchFromGeneratingToPlaying(MazeConfiguration config) {
        currentState = states[2];
        currentState.setMazeConfiguration(config);
        this.robot = new BasicRobot();
        this.setRobotAndDriver(robot, driver);
        robot.setMaze(this);
        currentState.start(this, panel);
    }
	
	@Test public void testHasStopped() {
		robot.setBatteryLevel(0);
		assertTrue(robot.hasStopped());
	}
	
	@Test public void testRotate() {
		robot.rotate(Turn.LEFT, false);
		assertEquals(robot.getBatteryLevel(), 0, 2997);
		robot.rotate(Turn.LEFT, false);
		assertEquals(robot.getBatteryLevel(), 0, 2994);
		robot.rotate(Turn.RIGHT, false);
		assertEquals(robot.getBatteryLevel(), 0, 2991);
		robot.rotate(Turn.RIGHT, false);
		assertEquals(robot.getBatteryLevel(), 0, 2988);
	}
}
