package generation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import generation.Order.Builder;

public class MazeFactoryTest{
	//construct general test case for all mazes regardless of generation under different algorithms
	//so we have the following java files as interfaces: MazeConfiguration, Order. Stub Order class is implemented 
	//transfer configurations of the maze for testing. 
	private MazeConfiguration manager;
	StubOrder fakeOrder;
	MazeFactory worker;
	Cells cell;
	//start with initializing maze and its characteristics
	
	@Before
	public void init() throws Exception{
		//first construct new maze (not deterministic)
		worker = new MazeFactory(false);
		fakeOrder = new StubOrder(Builder.Prim, 3, false);
		worker.order(fakeOrder);
		worker.waitTillDelivered(); //order is important.
		manager = fakeOrder.getSetUp();
	}
	
	@After 
	
	@Test public void testIsThereAPossibleExit(){
		//iterate through entire 2D array to find exit.
		Distance md = manager.getMazedists();
		int x = 0;
		int y = 0;
		cell = manager.getMazecells();
		int dist = 0; //initialize dist to find distance from exit
		for(int w = 0; w < manager.getWidth(); w++) {
			for(int h = 0; h < manager.getHeight(); h++) {
				if(md.getDistanceValue(w, h) == 1) {
					dist += 1;
					x = w;
					y = h;

				}
			}
		} assertEquals(1, dist);
		assertTrue(cell.isExitPosition(x, y)); //if this position of distance 1 is exit position
	}
	
		
	@Test public void testStartingPositionLongestDistanceFromExit() {
		Distance md = manager.getMazedists();
		int[] start = manager.getStartingPosition();
		int startx = start[0];
		int starty = start[1];
		int maxDist = manager.getDistanceToExit(startx, starty);
		int newMax = 0;
		for(int w = 0; w < manager.getWidth(); w++) {
			for(int h = 0; h < manager.getHeight(); h++) {
				if(md.getDistanceValue(w, h) > maxDist) {
					newMax = md.getDistanceValue(w, h);
				}
			}assertTrue(maxDist >= newMax); //THere should not be a new coordinate where it is the max dist from exit
		}
	}
}
