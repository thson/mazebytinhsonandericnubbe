package generation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import generation.Order.Builder;

public class MazeBuilderEllerTest {
	MazeConfiguration manager;
	StubOrder fakeOrder;
	MazeFactory worker;
	Cells cell;
	@Before
	public void init() throws Exception {
		worker = new MazeFactory(false);
		fakeOrder = new StubOrder(Builder.Eller, 4, false);
		worker.order(fakeOrder);
		worker.waitTillDelivered(); //order is important.
		manager = fakeOrder.getSetUp();
	}
	
	@After
	
	@Test public void testIsThereAPossibleExit(){
		//iterate through entire 2D array to find exit.
		Distance md = manager.getMazedists();
		int x = 0;
		int y = 0;
		cell = manager.getMazecells();
		int dist = 0; //initialize dist to find distance from exit
		for(int w = 0; w < manager.getWidth(); w++) {
			for(int h = 0; h < manager.getHeight(); h++) {
				if(md.getDistanceValue(w, h) == 1) {
					dist += 1;
					x = w;
					y = h;
				}
			}
		} assertEquals(1, dist);
		assertTrue(cell.isExitPosition(x, y)); //if this position of distance 1 is exit position
	}
	
	@Test public void testIsPerfect() {// test to see if there are no room
		boolean perfect = false;
		cell = manager.getMazecells();
		for(int x = 0; x < manager.getWidth(); x++) {
			for(int y = 0; y < manager.getHeight(); y++) {
				 if(cell.hasWall(x, y, CardinalDirection.East) && (cell.hasWall(x, y, CardinalDirection.West))
				    && (cell.hasWall(x, y, CardinalDirection.North) && (cell.hasWall(x, y, CardinalDirection.South)))){
					 perfect = !perfect;
						 }
			}
		} assertEquals(perfect, fakeOrder.isPerfect()); //This means that this algorithm guarantees perfect maze.
	}
}
