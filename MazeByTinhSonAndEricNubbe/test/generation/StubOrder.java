package generation;

 public class StubOrder implements Order{
	 
	 private Builder builder;
	 private int skill;
	 private boolean perfect; 
	 int progress;
	 private MazeConfiguration manager;
	 
	 public StubOrder(Builder builder, int skill, boolean perfect) {
		 this.builder = builder;
		 this.skill = skill;
		 this.perfect = perfect;
	 }

	@Override
	public int getSkillLevel() {
		return skill;
	}
	@Override
	public Builder getBuilder() {
		return builder;
	}
	@Override
	public void deliver(MazeConfiguration config) {
		this.manager = config;
	}
	public MazeConfiguration getSetUp() {
		return manager;
	}
	@Override
	public void updateProgress(int percent) {
		this.progress = percent;
	}
	public int getProgress() {
		return progress;
	}

	@Override
	public boolean isPerfect() {
		return perfect;
	}
}
