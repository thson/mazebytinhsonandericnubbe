package gui;

import generation.Distance;
import gui.Constants.UserInput;
import gui.Robot.Turn;

/**
 * This implementation of the RobotDriver requires input from the player to move
 * and cannot solve the maze itself. It only interacts with its Robot. The Robot
 * encapsulates all the information the Robot holds (i.e. battery, path length) and
 * interacts with the classes that move and draw the maze. 
 *
@Author Tinh Son and Eric Nubbe
*/
public class ManualDriver implements RobotDriver{
	private BasicRobot robot;
	private Turn turn;
	
	
	public ManualDriver(Robot r) {
		this.robot = (BasicRobot) r;
	}
	
	@Override
	public void setRobot(Robot r) {
		this.robot =  (BasicRobot) r;
	}

	@Override
	public void setDimensions(int width, int height) {
		
	}

	@Override
	public void setDistance(Distance distance) {
	}

	@Override
	public boolean drive2Exit() throws Exception {
		if(!robot.isOutsideMaze()) {
			return false;
		}
		return true;	
	}
	
	/**
	 * To move, the manual driver calls the  move method of its Robot, which
	 * in turn interacts with the Controller and State.
	 * @param Forward
	 */
	public void move(UserInput Forward) {
		robot.move(1, true);
	}
	
	/** 
	 * To rotate, the manual driver calls the move method of its Robot, which in turn
	 * interacts with the Controller and State.
	 * @param key
	 */
	public void rotate(UserInput key) {
		
		switch(key) {
		case Left: turn = Turn.LEFT;
					break;
		case Right: turn = Turn.RIGHT;
					break;
		case Down: turn = Turn.LEFT;
					robot.rotate(turn, true);
					break;
		default: break;
		}
		robot.rotate(turn, true);
	}

	@Override
	public float getEnergyConsumption() {
		return this.robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		return this.robot.getOdometerReading();
	}
	
}
