package gui;

import gui.Constants.StateGUI;

/**
 * Implements the screens that are displayed whenever the game is not in 
 * the playing state. The screens shown are the title screen, 
 * the generating screen with the progress bar during maze generation,
 * and the final screen when the game finishes.
 * 
 * @author Peter Kemper
 *
 */
public class MazeView {
	private float energyLevel;
	private int pathLength;
	private StateGenerating controllerState; // used for generating screen
	//private static MazePanel MazePanel = new MazePanel();
    
    public MazeView(StateGenerating c) {
        super();
        controllerState = c ;
    }
    public MazeView(float battery, int length) {
    	super();
    	controllerState = null;
    	this.energyLevel = battery;
    	this.pathLength = length;
    }
    /**
     * Draws the title screen, screen content is hard coded
     * @param panel holds the graphics for the off-screen image
     * @param filename is a string put on display for the file
     * that contains the maze, can be null
     */
    public void redrawTitle(MazePanel panel, String filename) {
    	WrapperGraphics g = panel.getBufferGraphics() ;
        if (null == g) {
            System.out.println("MazeView.redrawTitle: can't get graphics object to draw on, skipping redraw operation") ;
        }
        else {
            redrawTitle(g,filename);
        }
    }
    /**
     * Helper method for redraw to draw the title screen, screen is hard coded
     * @param  gc graphics is the off-screen image, can not be null
     * @param filename is a string put on display for the file
     * that contains the maze, can be null
     */
    private void redrawTitle(WrapperGraphics gc, String filename) {
        // produce white background
    	MazePanel.setGraphicsColor(gc, "white");
    	MazePanel.fillGraphicsRect(gc, 0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
        //gc.setColor(Color.white);
        //gc.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
        // write the title 
    	MazePanel.setGraphicsFont(gc, "largeBannerFont");
        //gc.setFont(largeBannerFont);
        WrapperFontMetrics fm = MazePanel.getWrapperFontMetrics(gc);
        MazePanel.setGraphicsColor(gc, "red");
        //gc.setColor(Color.red);
        centerString(gc, fm, "MAZE", 100);
        // write the reference to Paul Falstad
        MazePanel.setGraphicsColor(gc, "blue");
        MazePanel.setGraphicsFont(gc, "smallBannerFont");
        //gc.setColor(Color.blue);
        //gc.setFont(smallBannerFont);
        fm = MazePanel.getWrapperFontMetrics(gc);
        centerString(gc, fm, "by Paul Falstad", 160);
        centerString(gc, fm, "www.falstad.com", 190);
        // write the instructions
        MazePanel.setGraphicsColor(gc, "black");
        //gc.setColor(Color.black);
        if (filename == null) {
        	// default instructions
        	centerString(gc, fm, "To start, select a skill level, a builder, and a driver", 250);
        	//centerString(gc, fm, "(Press a number from 0 to 9,", 300);
        	//centerString(gc, fm, "or a letter from A to F)", 320);  	
        	}
        else {
        	// message if maze is loaded from file
        	centerString(gc, fm, "Loading maze from file:", 250);
        	centerString(gc, fm, filename, 300);
        }
        centerString(gc, fm, "Version 4.0", 350);
    }

	
	public void redraw(WrapperGraphics gc, StateGUI state, int px, int py, int view_dx,
			int view_dy, int walk_step, int view_offset, RangeSet rset, int ang) {
		//dbg("redraw") ;
		switch (state) {
		case STATE_TITLE:
			redrawTitle(gc,null);
			break;
		case STATE_GENERATING:
			redrawGenerating(gc);
			break;
		case STATE_PLAY:
			// skip this one
			break;
		case STATE_FINISH:
			redrawFinish(gc);
			break;
		case STATE_LOSING:
			redrawLosing(gc);
		}
	}
	
	private void dbg(String str) {
		System.out.println("MazeView:" + str);
	}
    /**
     * Draws the finish screen, screen content is hard coded
     * @param panel holds the graphics for the off-screen image
     */
	void redrawFinish(MazePanel panel) {
		WrapperGraphics g = panel.getBufferGraphics() ;
        if (null == g) {
            System.out.println("MazeView.redrawFinish: can't get graphics object to draw on, skipping redraw operation") ;
        }
        else {
            redrawFinish(g);
        }
	}
	/**
	 * Helper method for redraw to draw final screen, screen is hard coded
	 * @param gc graphics is the off-screen image
	 */
	void redrawFinish(WrapperGraphics gc) {
		// produce blue background
    	MazePanel.setGraphicsColor(gc, "blue");
    	MazePanel.fillGraphicsRect(gc, 0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		//gc.setColor(Color.blue);
		//gc.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		// write the title 
    	MazePanel.setGraphicsFont(gc, "largeBannerFont");
		//gc.setFont(largeBannerFont);
    	WrapperFontMetrics fm = MazePanel.getWrapperFontMetrics(gc);
        MazePanel.setGraphicsColor(gc, "yellow");
		//FontMetrics fm = gc.getFontMetrics();
		//gc.setColor(Color.yellow);
		centerString(gc, fm, "You won!", 100);
		fm = MazePanel.getWrapperFontMetrics(gc);
		// write some extra blurb
		MazePanel.setGraphicsColor(gc, "orange");
		MazePanel.setGraphicsFont(gc, "smallBannerFont");
		//gc.setColor(Color.orange);
		//gc.setFont(smallBannerFont);
		fm = gc.getFontMetrics();
		centerString(gc, fm, "Congratulations!", 160);
		//print battery level and pathLength
		MazePanel.setGraphicsFont(gc, "smallBannerFont");
		MazePanel.setGraphicsColor(gc, "yellow");
		//gc.setFont(smallBannerFont);
		//gc.setColor(Color.yellow);
		centerString(gc, fm, "Path length: " + pathLength, 200);
		MazePanel.setGraphicsColor(gc, "yellow");
		//gc.setColor(Color.yellow);
		centerString(gc, fm, "Battery level: " + energyLevel, 240);
		// write the instructions
		MazePanel.setGraphicsColor(gc, "white");
		//gc.setColor(Color.white);
		centerString(gc, fm, "Hit any key to restart", 300);
	}
	/**
     * Draws the finish screen, screen content is hard coded
     * @param panel holds the graphics for the off-screen image
     */
	void redrawLosing(MazePanel panel) {
		WrapperGraphics g = panel.getBufferGraphics() ;
        if (null == g) {
            System.out.println("MazeView.redrawLosing: can't get graphics object to draw on, skipping redraw operation") ;
        }
        else {
            redrawLosing(g);
        }
	}
	/**redrawLosing updates end screen with losing message
	 * @param gc
	 */
	void redrawLosing(WrapperGraphics gc){
		// produce red background
    	MazePanel.setGraphicsColor(gc, "red");
    	MazePanel.fillGraphicsRect(gc, 0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		// write the title 
    	MazePanel.setGraphicsFont(gc, "largeBannerFont");
    	WrapperFontMetrics fm = MazePanel.getWrapperFontMetrics(gc);
        MazePanel.setGraphicsColor(gc, "yellow");
		centerString(gc, fm, "You Lost!", 100);
		fm = MazePanel.getWrapperFontMetrics(gc);
		// write some extra blurb
		MazePanel.setGraphicsColor(gc, "orange");
		MazePanel.setGraphicsFont(gc, "smallBannerFont");
		fm = MazePanel.getWrapperFontMetrics(gc);
		centerString(gc, fm, "Congratulations, failure!", 160);
		//print battery level and pathLength
		MazePanel.setGraphicsFont(gc, "smallBannerFont");
		MazePanel.setGraphicsColor(gc, "yellow");
		centerString(gc, fm, "Path length: " + pathLength, 200);
		MazePanel.setGraphicsColor(gc, "yellow");
		centerString(gc, fm, "Battery level: " + energyLevel, 240);
		// write the instructions
		MazePanel.setGraphicsColor(gc, "white");
		//gc.setColor(Color.white);
		centerString(gc, fm, "Hit any key to restart", 300);
	}
    /**
     * Draws the generating screen, screen content is hard coded
     * @param panel holds the graphics for the off-screen image
     */
    public void redrawGenerating(MazePanel panel) {
    	WrapperGraphics g = panel.getBufferGraphics() ;
        if (null == g) {
            System.out.println("MazeView.redrawGenerating: can't get graphics object to draw on, skipping redraw operation") ;
        }
        else {
            redrawGenerating(g);
        }
    }
	/**
	 * Helper method for redraw to draw screen during phase of maze generation, screen is hard coded
	 * only attribute percent done is dynamic
	 * @param gc graphics is the off screen image
	 */
	void redrawGenerating(WrapperGraphics gc) {
		// produce yellow background
		MazePanel.setGraphicsColor(gc, "yellow");
		MazePanel.fillGraphicsRect(gc, 0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		//gc.setColor(Color.yellow);
		//gc.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		// write the title 
		MazePanel.setGraphicsFont(gc, "largeBannerFont");
		//gc.setFont(largeBannerFont);
		WrapperFontMetrics fm = MazePanel.getWrapperFontMetrics(gc);
		MazePanel.setGraphicsColor(gc, "red");
		//gc.setColor(Color.red);
		centerString(gc, fm, "Building maze", 150);
		MazePanel.setGraphicsFont(gc, "smallBannerFont");
		//gc.setFont(smallBannerFont);
		fm = MazePanel.getWrapperFontMetrics(gc);
		//fm = gc.getFontMetrics();
		// show progress
		MazePanel.setGraphicsColor(gc, "black");
		//gc.setColor(Color.black);
		if (null != controllerState) 
		    centerString(gc, fm, controllerState.getPercentDone()+"% completed", 200);
		else
			centerString(gc, fm, "Error: no controller, no progress", 200);
		// write the instructions
		centerString(gc, fm, "Hit escape to stop", 300);
	}
	
	private void centerString(WrapperGraphics g, WrapperFontMetrics fm, String str, int ypos) {
		MazePanel.drawGraphicsString(g, str, (Constants.VIEW_WIDTH-MazePanel.getFontMetricsStringWidth(fm, str))/2, ypos);
		//g.drawString(str, (Constants.VIEW_WIDTH-fm.stringWidth(str))/2, ypos);
	}

}
