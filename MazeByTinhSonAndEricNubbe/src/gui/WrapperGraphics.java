package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.lang.reflect.Field;

public class WrapperGraphics {

	Graphics graphics;
	Graphics2D graphics2D;
	Color color;
	Font font;

	
	public WrapperGraphics(Graphics graphics) {
		setGraphics(graphics);
	}
	
	public void setGraphics(Graphics graphics) {
		this.graphics = graphics;
	}
	
	public Graphics getGraphics() {
		return this.graphics;
	}
	
	public void fillRect(int x, int y, int width, int height) {
		this.graphics.fillRect(x, y , width, height);
	}
	
	public void setColor(String colorName) {
		try {
		    Field field = Class.forName("java.awt.Color").getField(colorName);
		    color = (Color) field.get(null);
		} catch (Exception e) {
		   color = null; 
		}
		
		this.graphics.setColor(color);
	}

	public void setColor(WrapperColor wc) {
		this.graphics.setColor(wc.getColor());
	}
	
	public void fillPolygon(int[] x, int[] y, int n) {
		this.graphics.fillPolygon(x,y,n);	
	}

	public void drawLine(int x1, int y1, int x2, int y2) {
		this.graphics.drawLine(x1,  y1 , x2, y2);
	}

	public void fillOval(int x, int y, int width, int height) {
		this.graphics.fillOval(x, y, width,  height);
	}

	public void setFont(String fontName) {
		
		if (fontName.equals("largeBannerFont")) {
			font =  new Font("TimesRoman", Font.BOLD, 48);
		}
		else if (fontName.equals("smallBannerFont")) {
			font = new Font("TimesRoman", Font.BOLD, 16);
		}
		this.graphics.setFont(font);
	}

	public WrapperFontMetrics getFontMetrics() {
		WrapperFontMetrics fontMet = new WrapperFontMetrics(this.graphics.getFontMetrics());
		return fontMet;
	}

	public void drawString(String str, int x, int y) {
		this.graphics.drawString(str, x , y);
	}
	
	
}
