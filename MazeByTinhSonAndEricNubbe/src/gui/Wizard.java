package gui;

import generation.Distance;
import generation.MazeConfiguration;
import gui.Robot.Turn;

/** Finds the exit of the maze by cheating and using the maze configuration to 
 * find the neighbor closest to exit and moving. THe fastest possible solver
 * @author Tinh Son and Eric Nubbe
 *
 */

public class Wizard implements RobotDriver {
	private BasicRobot robot;
	private MazeConfiguration mazeContainer;
	private Distance dist;
	
	public Wizard(Robot r) {
		this.robot = (BasicRobot) r;
	}
	
	@Override
	public void setRobot(Robot r) {
		this.robot = (BasicRobot) r;
	}

	@Override
	public void setDimensions(int width, int height) {
		
	}

	@Override
	public void setDistance(Distance distance) {
		
	}

	/*Pseudocode:
	Instantiate BasicRobot
	While not out of maze:
		Use Distance Object to find NeighborClosetToExit()
		Calls move method of BasicRobot
		Checks if RobotOut of Maze
	return energy, etc.*/
	@Override
	public boolean drive2Exit() throws Exception {
		this.mazeContainer = robot.getMazeConfiguration();
		this.dist = mazeContainer.getMazedists();
		try {
		int curX = robot.getCurrentPosition()[0];
		int curY = robot.getCurrentPosition()[1];
		while (!robot.isOutsideMaze()) {
			//check all four distance sensors using helper getClosetNeighbor()
			int [] neighbor = this.mazeContainer.getNeighborCloserToExit(curX, curY);
			//turn to appropriate direction
			if(curY > neighbor[1] && curX == neighbor[0]) {
				switch(robot.getCurrentDirection()) {
				case North:
					break;
				case South:
					robot.rotate(Turn.AROUND, false);
					break;
				case East:
					robot.rotate(Turn.RIGHT, false);
					break;
				case West:
					robot.rotate(Turn.LEFT,  false);
					break;
				default:
					break;
				} 
			

			}
			else if(curY < neighbor[1] && curX == neighbor[0]) {
				switch(robot.getCurrentDirection()) {
				case North:
					robot.rotate(Turn.AROUND, false);
					break;
				case South:
					break;
				case East:
					robot.rotate(Turn.LEFT, false);
					break;
				case West:
					robot.rotate(Turn.RIGHT, false);
					break;
				default: break;
				} 
			}
			else if(curX < neighbor[0] && curY == neighbor[1]) {
				switch(robot.getCurrentDirection()) {
				case North:
					robot.rotate(Turn.LEFT, false);
					break;
				case South:
					robot.rotate(Turn.RIGHT, false);
					break;
				case East:
					break;
				case West:
					robot.rotate(Turn.AROUND, false);
					break;
				default:
					break;
				} 
			}
			else if(curX > neighbor[0] && curY == neighbor[1]) {
				switch(robot.getCurrentDirection()) {
				case North:
					robot.rotate(Turn.RIGHT, false);
					break;
				case South:
					robot.rotate(Turn.LEFT, false);
					break;
				case East:
					robot.rotate(Turn.AROUND, false);
					break;
				case West:
					break;
				default:
					break;
		}
			}
			robot.move(1, false);
			curX = robot.getCurrentPosition()[0];
			curY = robot.getCurrentPosition()[1];
	}
		}
		catch (Exception e) {
			return true;
		}
		return true;
	}
	 
		
	@Override
	public float getEnergyConsumption() {
		return robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}

}
