package gui;

import java.awt.Color;
import java.lang.reflect.Field;

public class WrapperColor {
	
	Color color;

	public WrapperColor(String colorString) {
		setColor(colorString);
	}
	
	public WrapperColor(int red, int green, int blue) {
		setColor(red,green,blue);
	}
	
	public WrapperColor(int bits) {
		this.color = new Color(bits);
	}
	
	public void setColor(String colorString) {
		try {
		    Field field = Class.forName("java.awt.Color").getField(colorString);
		    this.color = (Color) field.get(null);
		} catch (Exception e) {
		   this.color = null; 
		}
	}
		
	public void setColor(int red, int green, int blue) {
		this.color = new Color(red,green,blue);
	}
	
	public void setColor(int bits) {
		this.color = new Color(bits);
	}
	
	public Color getColor() {
		return color;
	}

	public int getRGB() {
		return color.getRGB();
	}
}
