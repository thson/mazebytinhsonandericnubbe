package gui;

import java.awt.FontMetrics;

public class WrapperFontMetrics {
	
	FontMetrics fontMet;
	
	public WrapperFontMetrics(FontMetrics fontMetrics) {
		fontMet = fontMetrics;
	}

	public int getStringWidth(String str) {
		return this.fontMet.stringWidth(str);
	}

}
