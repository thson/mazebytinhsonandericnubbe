package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import gui.Constants.UserInput;

public class SelectionBoxes extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	public JComboBox<String> skillSelect;
	public JComboBox<String> builderSelect;
	public JComboBox<String> driverSelect;
	public JButton startButton;
	boolean isPressed;
	public String[] choices;
	public Controller controller;
	JPanel thePanel = new JPanel();
	
	
	public SelectionBoxes() {
		//make list of choices with three possibilities
		String[] choices = new String[]{"0", "DFS","Manual"};
		//Set layout so the components can be aligned vertically
		thePanel.setLayout(new BoxLayout(thePanel,BoxLayout.PAGE_AXIS));
		
		//The choices each combo box will have
		String[] skillLevel = {"0","1","2","3","4","5","6","7","8","9"};
		String[] builderAlgorithm = {"DFS","Prim","Eller"};
		String[] driverAlgorithm = {"Manual","Wizard","Wall Follower", "Explorer", "Pledge"};
		
		//Make the ComboBoxes using the lists
		JComboBox<String> skillSelect = new JComboBox<>(skillLevel);
		JComboBox<String> builderSelect = new JComboBox<>(builderAlgorithm);
		JComboBox<String> driverSelect= new JComboBox<>(driverAlgorithm);
				
		//Create a start button
		startButton = new JButton("Start");
		isPressed = false;
		
		//Clicking the button saves the selected choices in each ComboBox
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				choices[0] = (String) skillSelect.getSelectedItem();
				choices[1] = (String) builderSelect.getSelectedItem();
				choices[2] = (String) driverSelect.getSelectedItem();
				controller.setChoiceList(choices);
				controller.keyDown(UserInput.Start, Integer.parseInt(choices[0]));
				thePanel.setVisible(false);
			}
		});
				
		//adds the ComboBoxes and Button to the panel and spaces them apart
		thePanel.add(Box.createRigidArea(new Dimension(0,5)));
		thePanel.add(skillSelect);
		thePanel.add(builderSelect);
		thePanel.add(driverSelect);
		thePanel.add(startButton);

		//makes the panel visible
		thePanel.setVisible(true);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		//TODO
		}
	
	public void setController(Controller controller) {
		this.controller = controller;
	}
	
	public JPanel getPanel() {
		return this.thePanel;
	}
	
	public String[] getChoiceList() {
		return this.choices;
	}
	
	public boolean isButtonPressed() {
		return this.isPressed;
	}

	public void setButton(boolean b) {
		this.isPressed = b;
	}
	     
}
