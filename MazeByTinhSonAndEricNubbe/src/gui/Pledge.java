package gui;

import generation.Distance;
import gui.Robot.Direction;
import gui.Robot.Turn;

public class Pledge implements RobotDriver {
	private BasicRobot robot;
	private int totalTurns;
	
	public Pledge(Robot r) {
		this.robot = (BasicRobot) r;
	}
	
	@Override
	public void setRobot(Robot r) {
		this.robot = (BasicRobot) r;
	}

	@Override
	public void setDimensions(int width, int height) {
		
	}

	@Override
	public void setDistance(Distance distance) {
	}

			
	@Override
	public boolean drive2Exit() throws Exception {
		try {
			totalTurns = 0;
			while(!robot.isOutsideMaze()) {
				while (robot.distanceToObstacle(Direction.FORWARD) != 0) {
					robot.move(1, false);
				}
				if (robot.distanceToObstacle(Direction.FORWARD) == 0) {
					robot.rotate(Turn.RIGHT,  false);
					totalTurns ++;
				}
				while (totalTurns != 0) {
					if (robot.distanceToObstacle(Direction.LEFT) == 0) {
						if (robot.distanceToObstacle(Direction.FORWARD) != 0){
							robot.move(1, false);
							
						}
						else if (robot.distanceToObstacle(Direction.FORWARD) == 0){
							robot.rotate(Turn.RIGHT, false);
							totalTurns++;
						}
					}
					else if (robot.distanceToObstacle(Direction.LEFT) != 0) {
						robot.rotate(Turn.LEFT, false);
						totalTurns --;
						robot.move(1, false);
					} if(robot.hasStopped()) {
						break;
						}
					
					if(robot.isOutsideMaze()) {
						return true; 	
					}
				}
				if(robot.hasStopped()) {
					break;
					}
				}
			}
	 
		catch (Exception e) {
		}
		
		if(robot.isOutsideMaze()) {
			return true; 	
		}
		return false;
}
		
	@Override
	public float getEnergyConsumption() {
		return robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}

}