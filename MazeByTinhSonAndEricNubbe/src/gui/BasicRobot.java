package gui;

import generation.CardinalDirection;
import generation.Cells;
import generation.MazeConfiguration;


/** This class implements the Robot interface by creating a Robot object.
 * The Robot that this class creates is used by the RobotDriver and ManualDriver
 * classes to actually move the robot around the maze
 * 
 * @author Eric Nubbe and Tinh Son
 */

public class BasicRobot implements Robot{
	/* The Robot needs severals instance variables to actually function. 
	 */
	
	private float battery; //how much energy the robot has
	private Direction front;//the direction the robot is facing
	private int odometer;//total distance traveled
	private Controller control;//how the robot accesses the state of the maze
	private boolean roomsensor;//whether the robot can tell if it is in a room
	private MazeConfiguration config;//data about the maze
	private Cells cell;
	private boolean dsForward;
	private boolean dsBackward;
	private boolean dsRight;
	private boolean dsLeft;
	private StatePlaying playing;
	private boolean isOutside = false;
	/*Constructs  Robot object and sets its initial variables, such as battery level and odometer
	 */
	
	public BasicRobot() {
		this.odometer = 0; //has not moved
		this.roomsensor = true; //has room sensor
		this.dsForward = true; //distance sensor forward
		this.dsBackward = true; //distance sensor backward
		this.dsRight = true; //distance sensor right
		this.dsLeft = true; //distance sensor left
		this.front = Direction.RIGHT;
		this.setBatteryLevel(3000);
		this.resetOdometer();
	} 
	
	@Override
	public void rotate(Turn turn, boolean manual) {
		this.setBatteryLevel(battery - 3);
		this.hasStopped(); //checks if the robot is out of battery
			switch (turn) {
			case LEFT:
				if (manual == false) {
					playing.rotate(1);
				}
				convertCarDirToCurDir();
				break;
			case RIGHT:
				if (manual == false) {
					playing.rotate(-1);
				}
				convertCarDirToCurDir();
				break;
			case AROUND:
				if (manual == false) {
					playing.rotate(1);
					playing.rotate(1);
				}
				convertCarDirToCurDir();
				break;
				} 
			} 
		
	@Override
	public void move(int distance, boolean manual) {
		if(manual == true) {
			int i = 0;
			while (i < distance && this.getBatteryLevel() > 0) {
				this.setBatteryLevel(battery - this.getEnergyForStepForward());
				this.hasStopped();
				this.odometer += 1;
				i++;
			}			
		} else {
			int i = 0;
			while (i < distance && this.getBatteryLevel() > 0) {
				this.setBatteryLevel(battery - this.getEnergyForStepForward());
				this.hasStopped();
				playing.walk(1);
				this.odometer += 1;
				this.getCurrentPosition();
				i++;
			} 
      } 
	}

	@Override
	public int[] getCurrentPosition() {	
		int x = control.getCurrentPosition()[0];
		int y = control.getCurrentPosition()[1];
		try {
			if (config.isValidPosition(x,y)) {
				return control.getCurrentPosition();
			}
			throw new Exception();
		}
		catch(Exception e) {
			this.isOutside = true;
			return control.getCurrentPosition();
		}
	}

	@Override
	public void setMaze(Controller controller) {
		this.control = controller;
		this.config = controller.getMazeConfiguration();
		this.playing = controller.getPlayingState();
		this.cell = control.getMazeConfiguration().getMazecells();
		}
	 
	public MazeConfiguration getMazeConfiguration() {
		return this.config;
	} 
	@Override
	public boolean isAtExit() {
		int[] position;
		try {
			position = this.getCurrentPosition();
			int x = position[0];
			int y = position[1];
			if(cell.isExitPosition(x,y)){
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}

	@Override
	public boolean canSeeExit(Direction direction) throws UnsupportedOperationException {
		if (this.distanceToObstacle(direction) == Integer.MAX_VALUE) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isInsideRoom() throws UnsupportedOperationException {
		try {
			if (!this.hasRoomSensor()) {
				throw new UnsupportedOperationException();
				}
			int[] position = this.getCurrentPosition();
			int x = position[0];
			int y = position[1];
			return cell.isInRoom(x,y);
		}
		catch (UnsupportedOperationException e) {
			return false;
		}
		catch(Exception ex) {
			return false;
		}
	}

	@Override
	public boolean hasRoomSensor() {
		return roomsensor;
	}

	@Override
	public CardinalDirection getCurrentDirection() {
		return control.getCurrentDirection(); //uses Controller method to get current direction
	}

	@Override
	public float getBatteryLevel() {
		if(battery < 0) {
			battery = 0;
		}
		return battery;
	}

	@Override
	public void setBatteryLevel(float level) {
		this.battery = level;
	}

	@Override
	public int getOdometerReading() {
		return odometer;
	}

	@Override
	public void resetOdometer() {
		this.odometer = 0;
	}

	@Override
	public float getEnergyForFullRotation() {
		return 12; //reach 90 degree turn costs 3 energy
	}

	@Override
	public float getEnergyForStepForward() {
		return 5; //one step requires 5 energy
	}

	@Override
	public boolean hasStopped() {
		if (this.getBatteryLevel() <= 0) {
			control.switchFromPlayingToLosing(); 
			return true;
		}
		return false;
	}

	@Override
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
		try {
			if(this.hasDistanceSensor(direction)) {
					this.setBatteryLevel(battery - 1);
					this.hasStopped();
					convertCarDirToCurDir();
					switch(this.front) { //given the direction the robot is facing converted from Cardinal Directions
					case FORWARD: 
						switch(direction) { //given the direction that the sensor is facing
						case LEFT: return calDist(CardinalDirection.West);
						case RIGHT: return calDist(CardinalDirection.East);
						case FORWARD: return calDist(CardinalDirection.South);
						case BACKWARD: return calDist(CardinalDirection.North);
						}
						
					case BACKWARD:
						switch(direction) {
						case LEFT: return calDist(CardinalDirection.East);
						case RIGHT: return calDist(CardinalDirection.West);
						case FORWARD: return calDist(CardinalDirection.North);
						case BACKWARD: return calDist(CardinalDirection.South);
						}
					case LEFT:
						switch(direction) {
						case LEFT: return calDist(CardinalDirection.North);
						case RIGHT: return calDist(CardinalDirection.South);
						case FORWARD: return calDist(CardinalDirection.West);
						case BACKWARD: return calDist(CardinalDirection.East);
						}
					case RIGHT:
						switch(direction) {
						case LEFT: return calDist(CardinalDirection.South);
						case RIGHT: return calDist(CardinalDirection.North);
						case FORWARD: return calDist(CardinalDirection.East);
						case BACKWARD: return calDist(CardinalDirection.West);
						}
					default: break;
					}
				}
				throw new UnsupportedOperationException();
			} 
			catch(Exception e) {
				System.out.println(" Distance to Obstacle: Robot does not have sensor in that direction");
				return 0;
			} 
	}

	@Override
	public boolean hasDistanceSensor(Direction direction) {
		switch(direction){
			case FORWARD: return true; 
			case BACKWARD: return true;
			case RIGHT: return true;
			case LEFT: return true; 
			default: return true;
		}
	}
		
	/** Helper method to add distance sensors. Not useful for Project 3, 
	 * but good for overall design
	 * @param direction that sensor points
	 * @return nothing
	 * */
	public void addDistanceSensor(Direction direction) {
		switch(direction){
			case FORWARD: dsForward = true;
				break;
			case BACKWARD: dsBackward = true;
				break;
			case RIGHT: dsRight = true;
				break;
			case LEFT: dsLeft = true;
				break;
		}
	}
	/**Helper method used by distancetoObject. Given a cardinal direction,
	 * it iterates forward from the current position until it hits a wall.
	 * It returns the distance to that wall	 * 
	 * @param cardinalDirirection
	 * @return distance to wall in that direction
	 * @throws Exception
	 */
	public int calDist(CardinalDirection carDir) throws Exception {
		int dist = 0;
		try {
		int x = this.getCurrentPosition()[0];
		int y = this.getCurrentPosition()[1];
		while(cell.hasNoWall(x, y, carDir)) {
			dist += 1;
			switch(carDir) {
			case West: 
					if(x - 1 >= 0) {
						x -= 1;
					}
				break;
			case East: if(x + 1 < config.getHeight()) {
						x += 1;
				};
				break;
			case North: if(y + 1 < config.getWidth()) {
						y += 1;
			}
				break;
			case South:
				 if(y - 1 >= 0) {
					 y -= 1;
					}
				break;
			}
			 if (dist > config.getHeight() || dist > config.getWidth() ) {
				dist = Integer.MAX_VALUE;
				return dist;
			}
		  }
		}
		catch (Exception e) {
//			System.out.println("calDis: Not in maze ");
//			e.printStackTrace();
		}
	return dist;
	}
	/**
	 * Takes cardinal direction and convert it to relative facing direction for the robot. front, back, left, right are relative directions.
	 * Used in distance to obstacle since the robot can check in all directions, not just the direction it is facing.
	 * @param none
	 */
	public void convertCarDirToCurDir() {
		switch(this.getCurrentDirection()) {
		//behind, right, and left are for sanity check. 
		case West: this.front = Direction.LEFT; break;
		case East: this.front = Direction.RIGHT; break;
		case North: this.front = Direction.BACKWARD; break; 
		case South: this.front = Direction.FORWARD; break;
		default: this.front = Direction.RIGHT; 
		}
	}
	
	public boolean isOutsideMaze() {
		return isOutside;
	}
	
	public MazeConfiguration mazeConfig() {
		return config;
	}
	
	public Controller getController() {
		return control;
	}
	
	public Direction getFront() {
		return this.front;
	}
}

