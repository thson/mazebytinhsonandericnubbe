package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Panel;
import java.awt.RenderingHints;

/**
 * Add functionality for double buffering to an AWT Panel class.
 * Used for drawing a maze.
 * 
 * @author Peter Kemper
 *
 */
public class MazePanel extends Panel  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* Panel operates a double buffer see
	 * http://www.codeproject.com/Articles/2136/Double-buffer-in-standard-Java-AWT
	 * for details
	 */
	// bufferImage can only be initialized if the container is displayable,
	// uses a delayed initialization and relies on client class to call initBufferImage()
	// before first use
	private Image bufferImage;  
	private Graphics2D graphics; // obtained from bufferImage, 
	// graphics is stored to allow clients to draw on the same graphics object repeatedly
	// has benefits if color settings should be remembered for subsequent drawing operations
	
	/**
	 * Constructor. Object is not focusable.
	 */
	public MazePanel() {
		setFocusable(false);
		bufferImage = null; // bufferImage initialized separately and later
		graphics = null;	// same for graphics
	}
	
	@Override
	public void update(Graphics g) {
		paint(g);
	}
	/**
	 * Method to draw the buffer image on a graphics object that is
	 * obtained from the superclass. 
	 * Warning: do not override getGraphics() or drawing might fail. 
	 */
	public void update() {
		paint(getGraphics());
	}
	
	/**
	 * Draws the buffer image to the given graphics object.
	 * This method is called when this panel should redraw itself.
	 * The given graphics object is the one that actually shows 
	 * on the screen.
	 */
	@Override
	public void paint(Graphics g) {
		if (null == g) {
			System.out.println("MazePanel.paint: no graphics object, skipping drawImage operation");
		}
		else {
			g.drawImage(bufferImage,0,0,null);	
		}
	}

	/**
	 * Obtains a graphics object that can be used for drawing.
	 * This MazePanel object internally stores the graphics object 
	 * and will return the same graphics object over multiple method calls. 
	 * The graphics object acts like a notepad where all clients draw 
	 * on to store their contribution to the overall image that is to be
	 * delivered later.
	 * To make the drawing visible on screen, one needs to trigger 
	 * a call of the paint method, which happens 
	 * when calling the update method. 
	 * @return graphics object to draw on, null if impossible to obtain image
	 */
	public WrapperGraphics getBufferGraphics() {
		// if necessary instantiate and store a graphics object for later use
		if (null == graphics) { 
			if (null == bufferImage) {
				bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
				if (null == bufferImage)
				{
					System.out.println("Error: creation of buffered image failed, presumedly container not displayable");
					return null; // still no buffer image, give up
				}		
			}
			graphics = (Graphics2D) bufferImage.getGraphics();
			if (null == graphics) {
				System.out.println("Error: creation of graphics for buffered image failed, presumedly container not displayable");
			}
			else {
				// System.out.println("MazePanel: Using Rendering Hint");
				// For drawing in FirstPersonDrawer, setting rendering hint
				// became necessary when lines of polygons 
				// that were not horizontal or vertical looked ragged
				graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);
				graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
						RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			}
		}
		
		WrapperGraphics g = createWrapperGraphics(graphics);
		return g;
	}
	
	public static WrapperColor createWrapperColor(String colorname) {
		return new WrapperColor(colorname);
	}
	
	public static WrapperColor createWrapperColor(int red, int green, int blue) {
		return new WrapperColor(red, green, blue);
	}
	
	public static WrapperColor createWrapperColor(int bits) {
		return new WrapperColor(bits);
	}
	
	public static void setWrapperColorString(WrapperColor wc, String colorString) {
		wc.setColor(colorString);
	}
	
	public static void setWrapperColorRGB(WrapperColor wc, int red, int green, int blue) {
		wc.setColor(red, green, blue );
	}
	
	public static void setWrapperColorBits(WrapperColor wc, int bits) {
		wc.setColor(bits);
	}
	public static Color getWrapperColor(WrapperColor wc) {
		return wc.getColor();
	}
	
	public static int getWrapperRGB(WrapperColor wc) {
		return wc.getRGB();
	}
		
	public static WrapperGraphics createWrapperGraphics(Graphics2D graphics) {
		return new WrapperGraphics(graphics);
	}
	
	public static void fillGraphicsRect(WrapperGraphics wg, int x, int y, int width, int height) {
		wg.fillRect(x, y, width, height);
	}
	
	public static void setGraphicsColor(WrapperGraphics wg, String colorName) {
		wg.setColor(colorName);
	}
	
	public static void setGraphicsColor(WrapperGraphics wg, WrapperColor wc) {
		wg.setColor(wc);
	}
	
	public static void fillGraphicsPolygon(WrapperGraphics wg, int[] x, int[] y, int n) {
		wg.fillPolygon(x,y,n);
	}
	
	public static void drawGraphicsLine(WrapperGraphics wg, int x1, int y1, int x2, int y2 ) {
		wg.drawLine(x1, y1, x2, y2 );
	}
	
	public static void fillGraphicsOval(WrapperGraphics wg, int x, int y, int width, int height) {
		wg.fillOval(x, y, width, height);
	}
	
	public static void drawGraphicsString(WrapperGraphics wg, String str, int x, int y) {
		wg.drawString(str, x, y);
	}
	
	public static void setGraphicsFont(WrapperGraphics gc, String string) {
		gc.setFont(string);	
	}

	public static WrapperFontMetrics getWrapperFontMetrics(WrapperGraphics gc) {
		return gc.getFontMetrics();
	}

	public static int getFontMetricsStringWidth(WrapperFontMetrics fm, String str) {
		return fm.getStringWidth(str);
	}
		
}
