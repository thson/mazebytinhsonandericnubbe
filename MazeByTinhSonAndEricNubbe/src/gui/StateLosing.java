package gui;

import gui.Constants.UserInput;

/*
 * When the Robot runs out the energy without leaving the maze, the state of the game is "Losing"
 * The Robot calls on the Controller to update to this Losing state. This class removes the maze and 
 * calls on MazeView to draw a message for the player informing them that they have lost.
 * 
 * @author Tinh Son and Eric Nubbe
 */
public class StateLosing extends DefaultState {
	    MazeView view;
	    MazePanel panel;
	    Controller control;
	    RobotDriver driver;
	    
	    boolean started;
	    int pathLength;
	    
	    public StateLosing() {
	        pathLength = 0;
	        started = false;
	    }
	    
	    /**
	     * Start the game by showing the final screen with a winning message.
	     * @param controller needed to be able to switch states, must be not null
	     * @param panel is the UI entity to produce the screen on 
	     */
	    
	    public void start(Controller controller, MazePanel panel) {
	        started = true;
	        // keep the reference to the controller to be able to call method to switch the state
	        control = controller;
	        this.driver = control.getDriver();
	        // keep the reference to the panel for drawing
	        this.panel = panel;
	        // init mazeview, controller not needed for final screen
	        view = new MazeView(driver.getEnergyConsumption(), driver.getPathLength());

	        if (panel == null) {
	    		System.out.println("StateWinning.start: warning: no panel, dry-run game without graphics!");
	    		return;
	    	}
	        // otherwise show finish screen with winning message
	        // draw content on panel
	        view.redrawLosing(panel);
	        // update screen with panel content
	        panel.update();

	    }
	    
	    /**
	     * Method incorporates all reactions to keyboard input in original code, 
	     * The simple key listener calls this method to communicate input.
	     * Method requires {@link #start(Controller, MazePanel) start} to be
	     * called before.
	     * @param key provides the feature the user selected
	     * @param value is not used, exists only for consistency across State classes
	     */
	    public boolean keyDown(UserInput key, int value) {
	        if (!started)
	            return false;
	        // for any keyboard input switch to title screen
	        control.switchToTitle();    
	        return true;
	    }

	    @Override
	    public void setPathLength(int pathLength) {
	        this.pathLength = pathLength;
	    }
	    
	}