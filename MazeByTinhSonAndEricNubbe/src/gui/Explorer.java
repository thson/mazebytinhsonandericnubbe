package gui;

import java.util.Random;

import generation.CardinalDirection;
import generation.Distance;
import gui.Robot.Turn;
/** This implementation of the Driver automatically searches for the exit of the maze by moving to its neighbor
 * that is has visited least. Ties are broken randomly. Horrible at solving mazes
* @author Tinh Son and Eric Nubbe
* */


public class Explorer implements RobotDriver {
	private BasicRobot robot;
	private int[][] maze_array;
	private int x; 
	private int y;

	  
	public Explorer(Robot r) {
		this.robot = (BasicRobot) r;
	}
	@Override
	public void setRobot(Robot r) {
		this.robot = (BasicRobot) r;
	}

	@Override
	public void setDimensions(int width, int height) {
		this.maze_array = new int[width][height];
	}

	@Override
	public void setDistance(Distance distance) {
	}
	
	/*Psuedocode
	Instantiate a 2D array with size of maze
	Marks its current position in the array
	While not at exit(
		if exit visible:
		 	move to exit
		if not in room:
			Use DistanceToObstacle() to see which direction it can move forward
			Move to neighboring square least visited (randomly choose in case of tie)
			mark the position with number of times it has visited the position as it moves. 
		if in room:
			Look for all door cells
			Move to door that has been visited least 
		check if at exit*/
	@Override
	public boolean drive2Exit() throws Exception {
		setDimensions(robot.getController().getMazeConfiguration().getWidth(), robot.getController().getMazeConfiguration().getHeight());
		while (!robot.isAtExit()) {
			if(robot.hasStopped()) {
				break;
			}
			this.x = robot.getCurrentPosition()[0];
			this.y = robot.getCurrentPosition()[1];

			int[] move_dir = getSquareValues();
			moveToSquare(move_dir);
		} if(robot.hasStopped()) {
			return false;
		}
		return true;
	  } 

	@Override
	public float getEnergyConsumption() {
		return robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}
	
	public void moveToSquare(int[] move_dir) {
		int least_visited = 0;
		while(move_dir[least_visited] < 0) { //make least visited first number that is not -1
			least_visited ++;
			}
		int min = move_dir[least_visited];
		for (int i = least_visited + 1; i < move_dir.length; i++) {
			if (move_dir[i] < min && move_dir[i] >= 0) {
				min = move_dir[i];
				least_visited = i;
				}
			else if (move_dir[i] == min) {
			    Random random = new Random();
			    boolean rand = random.nextBoolean();
			    if (rand) { least_visited = i;}
					}
				}
		
		switch (least_visited) {
		case 0://North
			switch(robot.getFront()) {
			case BACKWARD: walk();
							break;
			case FORWARD: robot.rotate(Turn.AROUND, false);
						  robot.move(1,false);
						  break;
			case LEFT: robot.rotate(Turn.LEFT, false);
					   walk();
					   break;
			case RIGHT:robot.rotate(Turn.RIGHT, false);
			   		   walk();
			   		   break;
			default: break;
			}
			break;
			
		case 1: //South 
			switch(robot.getFront()) {
			case BACKWARD: robot.rotate(Turn.AROUND, false);
						   walk();
						   break;
			case FORWARD: robot.move(1,false);
						  break;
			case LEFT: robot.rotate(Turn.RIGHT, false);
			   		   walk();
			   		   break;
			case RIGHT:robot.rotate(Turn.LEFT, false);
				   	   walk();
				   	   break;
			default: break;
			} 
			break;
			
		case 2: 	//West
			switch(robot.getFront()) {
			case BACKWARD: robot.rotate(Turn.RIGHT, false);
						   walk();
						   break;
			case FORWARD: robot.rotate(Turn.LEFT, false);
						  robot.move(1,false);
						  break;
			case LEFT: walk();
			   		   break;
			case RIGHT:robot.rotate(Turn.AROUND, false);
				   	   walk();
				   	   break;
			default: break;
			} 
			break;
			
		case 3: 		//East
			switch(robot.getFront()) {
			case BACKWARD: robot.rotate(Turn.LEFT, false);
						   walk();
						   break;
			case FORWARD: robot.rotate(Turn.RIGHT, false);
						  robot.move(1,false);
						  break;
			case LEFT: robot.rotate(Turn.AROUND, false);
					   walk();
			   		   break;
			case RIGHT:walk();
				   	   break;
			default: break;
			} 
			break;
			
		default: break;
		}
		
		maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;

			}
	
	public int[] getSquareValues() {

		int[] move_dir = new int[4]; 
		for (int i = 0; i < 4; i++) {
			move_dir[i] = -1;
			}

		if (!robot.getController().getMazeConfiguration().hasWall(x,y,CardinalDirection.North)) {
			if (y-1 >= 0) {
				move_dir[0] = maze_array[x][y-1];
				}
			}
		if (!robot.getController().getMazeConfiguration().hasWall(x,y,CardinalDirection.South)) {
			if (y+1 <= robot.getMazeConfiguration().getHeight()) {
				move_dir[1] = maze_array[x][y+1];
				}
			}
		if (!robot.getController().getMazeConfiguration().hasWall(x,y,CardinalDirection.West)){
			if (x-1 >=0) {
				move_dir[2] = maze_array[x-1][y];
				}
			}
		if (!robot.getController().getMazeConfiguration().hasWall(x,y,CardinalDirection.East)) {
			if (x+1 <= robot.getMazeConfiguration().getWidth()) {
				move_dir[3] = maze_array[x+1][y];
				}
			} return move_dir;
		
	}

	//ROOM CASE SIMPLY DOESN'T WORK FOR US.
//	public ArrayList<Integer> markRoom() {
//		ArrayList<Integer> door_list = new ArrayList<Integer>(); //keeps track of doors
//		int startx = robot.getCurrentPosition()[0];
//		int starty = robot.getCurrentPosition()[1];
//		try {
//			if(robot.distanceToObstacle(Direction.FORWARD) == 0){
//				robot.rotate(Turn.RIGHT, false);
//			}
//		} catch (UnsupportedOperationException e) {
//			e.printStackTrace();
//		}
//		//record number of visits of door just passed through and position of door 
//		if (robot.getFront() == Direction.FORWARD) {
//			System.out.println("Entered from FORWARD");
//			door_list.add(maze_array[x][y-1]);
//			maze_array[x][y-1]++;
//		}
//		else if (robot.getFront() == Direction.BACKWARD) {
//			System.out.println("Entered from BACKWARD");
//			door_list.add(maze_array[x][y+1]);
//			maze_array[x][y +1]++;
//
//		}
//		else if (robot.getFront() == Direction.RIGHT) {
//			System.out.println("Entered from RIGHT");
//			door_list.add(maze_array[x-1][y]);
//			maze_array[x-1][y]++;
//
//		}
//		else if (robot.getFront() == Direction.LEFT) {
//			System.out.println("Entered from LEFT");
//			door_list.add(maze_array[x+1][y]);
//			maze_array[x+1][y]++;
//		}
//		
//		//turn left, follow wall
//		try {
//			if(robot.distanceToObstacle(Direction.LEFT) != 0){
//				System.out.println("Begin Marking Room. Turn Left");
//				robot.rotate(Turn.LEFT, false);
//			}
//		} catch (UnsupportedOperationException e) {
//			e.printStackTrace();
//		}
//		walk();
//		System.out.println("Starting Position: " + startx + "," + starty);
//		System.out.println("Current Position: " + robot.getCurrentPosition()[0] + "," + robot.getCurrentPosition()[1]);
//		//record starting position
//		while (robot.getCurrentPosition()[0] != startx || robot.getCurrentPosition()[1] != starty) {
//			System.out.println("Walking around the room");
//			//if door, mark how many times visited
//			try {
//				x = robot.getCurrentPosition()[0];
//				y = robot.getCurrentPosition()[1];
//				if(robot.distanceToObstacle(Direction.LEFT) != 0){
//					System.out.println("Marked a door");
//					switch(robot.getFront()) {
//					case BACKWARD: door_list.add(maze_array[x+1][y]);
//						System.out.println("door Value: " + maze_array[x + 1][y]);
//						maze_array[x][y]++;
//						break;
//					case FORWARD: door_list.add(maze_array[x-1][y]);
//						System.out.println("door Value: " + maze_array[x - 1][y]);
//						maze_array[x][y]++;
//						break;
//					case LEFT: door_list.add(maze_array[x][y-1]);
//						System.out.println("door Value: " + maze_array[x][y - 1]);
//						maze_array[x][y]++;
//						break;
//					case RIGHT:door_list.add(maze_array[x][y+1]);
//						System.out.println("door Value: " + maze_array[x][y + 1]);
//						maze_array[x][y]++;
//						break;
//					default:
//						break;
//					}	
//				}
//			} catch (UnsupportedOperationException e) {
//				System.out.println("Something's wrong with check doors");
//				e.printStackTrace();
//			}
//			
//			//robot walks into wall, turns right
//			try {
//				if (robot.distanceToObstacle(Direction.FORWARD) == 0) {
//					System.out.println("Hitting walk in room, turn right");
//					robot.rotate(Turn.RIGHT, false);	
//				}
//			} catch (UnsupportedOperationException e) {
//				e.printStackTrace();
//			}
//			try {
//				if(!robot.isInsideRoom()) {
//					break;
//				}
//			} catch (UnsupportedOperationException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			//walks out of room before explore all doors
////		    try {
////				if(!robot.isInsideRoom() && robot.distanceToObstacle(Direction.FORWARD) != 0) {
////					System.out.println("Just walked out of room without exploring entire perimeter. Returning");
////					if (!robot.isInsideRoom()) {
////						door_list.add(maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]);
////						robot.rotate(Turn.AROUND, false);
////							walk();
////						robot.rotate(Turn.LEFT, false);
////						}
////				}
////			} catch (UnsupportedOperationException e) {
////				System.out.println("robot.line 286");
////				e.printStackTrace();
////			} walk();
//			if(robot.roomBorder()) {
//				System.out.println("About to step out of room, turning right");
//				robot.rotate(Turn.RIGHT, false);
//			}
//			walk();
//		} 
//			return door_list;
//	}
//	
//	public void moveOut(ArrayList<Integer> door_list) {
		//once back at starting position, go until door is found again
//		int lowest = Collections.min(door_list);
//		try {
//			while(robot.isInsideRoom()){
////				System.out.println("Finished checking Room. Deciding");
//				System.out.println("Lowest Value: "+ lowest);
//				x = robot.getCurrentPosition()[0];
//				y = robot.getCurrentPosition()[1];
//				if(robot.distanceToObstacle(Direction.LEFT) == 0 && robot.distanceToObstacle(Direction.FORWARD) == 0){
//					robot.rotate(Turn.RIGHT, false);
//				} else if(robot.distanceToObstacle(Direction.FORWARD) == 0) {
//					robot.rotate(Turn.RIGHT, false);
//				}
//				walk();
//				if(robot.roomBorder()){
//					
//					switch(robot.getFront()) {
//					case BACKWARD: if (maze_array[x + 1][y] <= lowest) {
//						System.out.println("Going to walk out this door in front1");
//						walk();
//						maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;
//						walk();
//					}
//						break;
//					case FORWARD: if (maze_array[x - 1][y] <= lowest) {
//						System.out.println("Going to walk out this door in front2");
//						walk();
//						maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;
//						walk();
//
//					}
//						break;
//					case LEFT: if (maze_array[x][y - 1] <= lowest) {
//						System.out.println("Going to walk out this door in front3");
//						walk();
//						maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;
//						walk();
//
//					}
//						break;
//					case RIGHT: if (maze_array[x][y+ 1] <= lowest) {
//						System.out.println("Going to walk out this door in front4");
//						walk();
//						maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;
//						walk();
//					}
//						break;
//					default: 
//						break;
//					} 				}
//				if(robot.distanceToObstacle((Direction.LEFT)) != 0){
//						System.out.println("Checking Left-Side for Lowest value door");
//						switch(robot.getFront()) {
//						case BACKWARD: 
//							System.out.println("door Value: " + maze_array[x + 1][y]);
//							if (maze_array[x + 1][y] <= lowest) {
//							System.out.println("Leaving from BACKWARD");
//							robot.rotate(Turn.LEFT, false);
//							walk();
//							maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;
//							walk();
//						} 
//							break;
//						case FORWARD: 
//							System.out.println("door Value: " + maze_array[x - 1][y]);
//							if (maze_array[x - 1][y] <= lowest) {
//							System.out.println("Leaving from FORWARD");
//							robot.rotate(Turn.LEFT, false);
//							walk();
//							maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;
//							walk();
//						}
//							break;
//						case LEFT: 
//							System.out.println("door Value: " + maze_array[x][y-1]);
//							if (maze_array[x][y - 1] <= lowest) {
//							System.out.println("Leaving from LEFT");
//							robot.rotate(Turn.LEFT, false);
//							walk();
//							maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;
//							walk();
//						} 
//							break;
//						case RIGHT:
//							System.out.println("door Value: " + maze_array[x][y+1]);
//							if (maze_array[x][y+ 1] <= lowest) {
//							System.out.println("Leaving from RIGHT");
//							robot.rotate(Turn.LEFT, false);
//							walk();
//							maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;
//							walk();
//						}
//							break;
//						default: 
//							robot.rotate(Turn.LEFT, false);
//							walk();
//							maze_array[robot.getCurrentPosition()[0]][robot.getCurrentPosition()[1]]++;
//							walk();
//							break;
//						}  	
//					} 
//				}
//		} catch (UnsupportedOperationException e) {
//			System.out.println("Error in moveOut");
//			e.printStackTrace();
//		}
//	}
	
	public void walk() {
		robot.move(1, false);;
	}

}