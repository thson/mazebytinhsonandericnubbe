package generation;

import java.util.Random;
//Tag2.0
//This file did not reflect the work progress made since the project on gitlab was deleted and renamed, so that the commits made for the previous project was all gone. 
public class MazeBuilderEller extends MazeBuilder implements Runnable {
	private int currentValue = 0;
	private Random rand = new Random();
	public MazeBuilderEller() {
		super();
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}

	public MazeBuilderEller(boolean det) {
		super(det);
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}
	
	//Realized we don't have to set any walls up. All we need to do is to remove walls, so the steps down here are mmodified accordingly. 
	
	/* Generates pathways through a maze based on the following rule from this website: http://www.neocomputer.org/projects/eller.html
	    1. Create the first row. No cells will be members of any set
	
	    2. Join any cells not members of a set to their own unique set
	
	    3. Create right-walls, moving from left to right:
	        Randomly decide to add a wall or not
	            If the current cell and the cell to the right are members of the same set, always create a wall between them. (This prevents loops)
	            If you decide not to add a wall, union the sets to which the current cell and the cell to the right are members.
	
	    4. Create bottom-walls, moving from left to right:
	        Randomly decide to add a wall or not. Make sure that each set has at least one cell without a bottom-wall (This prevents isolations)
	            If a cell is the only member of its set, do not create a bottom-wall
	            If a cell is the only member of its set without a bottom-wall, do not create a bottom-wall
	
	    5. Decide to keep adding rows, or stop and complete the maze
	        If you decide to add another row:
	            Output the current row
	            Remove all right walls
	            Remove cells with a bottom-wall from their set
	            Remove all bottom walls
	            Continue from Step 2
	
	        If you decide to complete the maze
	            Add a bottom wall to every cell
	            Moving from left to right:
	                If the current cell and the cell to the right are members of a different set:
	                    Remove the right wall
	                    Union the sets to which the current cell and cell to the right are members.
	                    Output the final row.

	 */
	@Override
	protected void generatePathways() {
		int[] construct = new int[width]; //start with creating array of size width 
		for(int hcounter = 0; hcounter < height; hcounter++) {
			assignValue(hcounter, construct);
			cleanUp(hcounter, construct);
		} 
	}
	/**Algorithm used to remove east walls and south walls.
	*@param hcounter, array construct
	*@return constructed with some walls removed
	**/
	private int[] cleanUp(int hcounter, int[] construct) {
		//east walls
		for(int i = 0; i < width; i++) { //looping through construct
//			check last row. 
//			 If the current cell and the cell to the right are members of a different set:
//	         Remove the right wall
//	         Union the sets to which the current cell and cell to the right are members.
			if(hcounter == height - 1) {
				if(i + 1 < width && construct[i] != construct[i + 1]) {
					tearThatWall(i, hcounter, 3);
				}
			} else {
//		     Randomly decide to add a wall (remove in this case) or not
//		     If the current cell and the cell to the right are members of the same set, always create a wall between them. (This prevents loops)
//		     If you decide not to add a wall, union the sets to which the current cell and the cell to the right are members.
				if(i + 1 < width && construct[i] != construct[i + 1] && membershipApproval()) {
					tearThatWall(i, hcounter, 3);
					construct[i + 1] = construct[i];
			  }
		 }
	} 
		//now to south walls
		int alreadyTornDown = 0;
		for(int j = 0; j < width; j++) {
			if(hcounter == height - 1) { return construct;} //done.
			if(j == width - 1 || construct[j] != construct[j + 1]) {
				int whichWall = random.nextIntWithinInterval(alreadyTornDown, j);
				tearThatWall(whichWall, hcounter, 2);
				alreadyTornDown = j + 1; //next index marked for next tear down
			}
		} return construct;
	}
	
	/**assign membership to each cell
	 * @param hcounter, array construct
	 * @return construct membership value for each cell
	 */
	private int[] assignValue(int hcounter, int[] construct) {
		//reassignment of sets with top wall
		for(int Walls = 0; Walls < width; Walls++) {
			if(hcounter == 0 || cells.hasWall(Walls, hcounter, CardinalDirection.North)) { 
				construct[Walls] = currentValue;
				currentValue += 1;
			}
		} return construct;
	}
	
	private boolean membershipApproval() { //decides whether to include cell into previous set.
		int rng = rand.ints(0, 100).findFirst().getAsInt();
		if(rng < 50) {
			return true;
		} return false;
	}	
	
	/**encapsulated methods to break wall
	 * @param r, c, cardinal direction (cd)
	 */
	private void tearThatWall(int r, int c, int cd) {
		switch(cd) {
		case 1:
			cells.deleteWall(new Wall(r, c, CardinalDirection.North));
			break;
		case 2:
			cells.deleteWall(new Wall(r, c, CardinalDirection.South));
			break;
		case 3:
			cells.deleteWall(new Wall(r, c, CardinalDirection.East));
			break;
		case 4:
			cells.deleteWall(new Wall(r, c, CardinalDirection.West));
			break;
	    } 
	}
}