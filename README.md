FOR PROJECT 4, PRESS Q TO SOLVE MAZE AFTER SELECT SOLVER.
Appologies for the confusion, but please use this latest commit for project 4 and 5 grades. We guaranteed that we have more than 10 commits for each project. You could go back to our previous commits to check refactoring.
Also, for testing, please change Thread.sleep(25) to Thread.sleep(1) in StatePlaying for faster algo testing.
EXPLORER TESTS TAKE VERY LONG TO COMPLETE.